import os
import json
import tweepy
import boto3

CONSUMER_KEY=os.environ.get('CONSUMER_KEY', 'consumer-key')
CONSUMER_SECRET=os.environ.get('CONSUMER_SECRET', 'consumer-secret')
ACCESS_TOKEN=os.environ.get('ACCESS_TOKEN', 'access_token')
ACCESS_TOKEN_SECRET=os.environ.get('ACCESS_TOKEN_SECRET', 'access-token-secret')
STREAM_NAME=os.environ.get('FIREHOSE_STREAM_NAME', 'firehose-stream-name')
HASHTAG=os.environ.get('HASHTAG', 'hashtag')

client = boto3.client('firehose')
auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
api = tweepy.API(auth,
                 wait_on_rate_limit=True)

def lambda_handler():
    tweets = tweepy.Cursor(api.search,
                           q=HASHTAG,
                           recent=True,
                           count=500,
                           tweet_mode='extended',
                           lang='pt-br').items(1000)
    for tweet in tweets:
        row = dict(
            id=tweet.id,
            user_id=tweet.user.id,
            user_name=tweet.user.name,
            tweet=str(tweet.full_text).replace('\n', ''),
            source=tweet.source,
            created_at=str(tweet.created_at)
        )

        client.put_record(
            DeliveryStreamName=STREAM_NAME,
            Record={'Data': json.dumps(row)}
        )

    return True
